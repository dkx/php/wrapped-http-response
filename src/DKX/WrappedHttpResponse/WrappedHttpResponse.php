<?php

declare(strict_types=1);

namespace DKX\WrappedHttpResponse;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

abstract class WrappedHttpResponse implements ResponseInterface
{


	/** @var \Psr\Http\Message\ResponseInterface */
	protected $innerResponse;


	/**
	 * @param \Psr\Http\Message\ResponseInterface $innerResponse
	 */
	public function __construct(ResponseInterface $innerResponse)
	{
		$this->innerResponse = $innerResponse;
	}


	public function getProtocolVersion()
	{
		return $this->innerResponse->getProtocolVersion();
	}


	public function withProtocolVersion($version)
	{
		return new static($this->innerResponse->withProtocolVersion($version));
	}


	public function getHeaders()
	{
		return $this->innerResponse->getHeaders();
	}


	public function hasHeader($name)
	{
		return $this->innerResponse->hasHeader($name);
	}


	public function getHeader($name)
	{
		return $this->innerResponse->getHeader($name);
	}


	public function getHeaderLine($name)
	{
		return $this->innerResponse->getHeaderLine($name);
	}


	public function withHeader($name, $value)
	{
		return new static($this->innerResponse->withHeader($name, $value));
	}


	public function withAddedHeader($name, $value)
	{
		return new static($this->innerResponse->withAddedHeader($name, $value));
	}


	public function withoutHeader($name)
	{
		return new static($this->innerResponse->withoutHeader($name));
	}


	public function getBody()
	{
		return $this->innerResponse->getBody();
	}


	public function withBody(StreamInterface $body)
	{
		return new static($this->innerResponse->withBody($body));
	}


	public function getStatusCode()
	{
		return $this->innerResponse->getStatusCode();
	}


	public function withStatus($code, $reasonPhrase = '')
	{
		return new static($this->innerResponse->withStatus($code, $reasonPhrase));
	}


	public function getReasonPhrase()
	{
		return $this->innerResponse->getReasonPhrase();
	}

}
